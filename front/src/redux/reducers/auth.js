import auth from '../states/authState'

export default function authReducer(state=auth,action){
    const tmp = {...state}
        if(action.type==='signupChange'){
            tmp.signup[action.key]=action.value
            return tmp
        }

        if(action.type==='signinChange'){
            tmp.signin[action.key]=action.value
            return tmp
        }

        if(action.type==='checkEmpty'){
            tmp.error=`${action.key} is empty`
            
            return tmp
        }

        if(action.type==='signupError'){
            if('error' in action.error){
                tmp.error=action.error.error
                return tmp
            }else if('success' in action.error){
                tmp.error=action.error.error
                return tmp
            }
            
            
        }


    
    return tmp
}