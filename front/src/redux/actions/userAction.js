import axios from 'axios'

export function signupChange(key,value){
    return{
        type:'signupChange',
        key,
        value
    }
}

export function signinChange(key,value){
    return{
        type:'signinChange',
        key,
        value
    }
}

export function validateSignup(signup){

    return (dispatch)=>{
        axios.post('http://localhost:5000/api/register', signup)
            .then(r=>{
                console.log(r.data)
            })
            .catch(error=>{
               console.log(error.response.data)
            })
        // const re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        // for(let key in signup){
        //     if(!signup[key]){
        //         dispatch(checkEmpty(key))
        //         break
        //     }else if(!re.test(signup.email) ){
        //         dispatch(signupError({error:'Fill in valid mail'}))
        //         break
        //     }else{
        //         dispatch(signupError({success:'success'}))
        //         axios.post('http://localhost:5000/api/register', signup)
        //         break
        //     }
        // }
    }
}

export function signupError(error){
    return{
        type:'signupError',
        error 
    }
}

export function checkEmpty(key){
    return{
        type:'checkEmpty',
        key
    }
}