import React from 'react'
import { Switch, Route, BrowserRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import LoginComponent from './components/loginPage/LoginComponent'
 

function App(){
  return(
    <BrowserRouter>
       <Route exact path='/' component={LoginComponent}/>
    </BrowserRouter>
   )
}

export default connect(r=>r)(App)