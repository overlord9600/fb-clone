import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { signinChange, signupChange, validateSignup } from '../../redux/actions/userAction';
import './login.css'
function Login(props){

    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };
     return(
        <div className='container'>
            <div className='col'>
                <div className='logo'>
                    <img src='/images/logo.svg'/>
                     
                </div>
                
            </div>
            <div className='col'>
                <div className='login-box'>
                    <div className='inps'>
                        <input placeholder='Phone number or email' className='textField' value={props.auth.signin.name} onChange={(e)=>props.dispatch(signinChange('login',e.target.value))}/>
                    </div>
                    <div className='inps'>
                        <input placeholder='password' className='textField' value={props.auth.signin.name} onChange={(e)=>props.dispatch(signinChange('password',e.target.value))}/>
                    </div>
                    
                    <button className='login-btn'>Log in</button>
                    <br/>
                    <a href='/'>forgot your password?</a>
                    <hr/>
                    <button className='account-btn' onClick={handleClickOpen}>Create new account</button>
                    <Dialog
                      open={open}
                      onClose={handleClose}
                      aria-labelledby="alert-dialog-title"
                      aria-describedby="alert-dialog-description"
                    >
                      <DialogTitle id="alert-dialog-title">{"Create new account"}</DialogTitle>
                      <DialogContent>
                        
                        <div className='register-box'>
                             <div className='form'>
                             <p>{props.auth.error}</p>
                                <div className='firstLine'>
                                    <div>
                                       <input placeholder='name' value={props.auth.signup.name} onChange={(e)=>props.dispatch(signupChange('name',e.target.value))}/>
                                    </div>
                                    <div>
                                        <input placeholder='surname' value={props.auth.signup.surname} onChange={(e)=>props.dispatch(signupChange('surname',e.target.value))}/>
                                    </div>
                                </div>
                                <div className='secLine'>
                                    <div>
                                       <input placeholder='Phone number or email' value={props.auth.signup.login} onChange={(e)=>props.dispatch(signupChange('login',e.target.value))}/>
                                    </div>
                                    <div>
                                        <input placeholder='Password' value={props.auth.signup.password} onChange={(e)=>props.dispatch(signupChange('password',e.target.value))}/>
                                    </div>
                                </div>
                                <button className='account-btn' onClick={()=>props.dispatch(validateSignup(props.auth.signup))}>Sign up</button>
                             </div>

                        </div>
                      </DialogContent>
                      <DialogActions>
                        <Button onClick={handleClose} color="primary" autoFocus>
                          Close
                        </Button>
                      </DialogActions>
                    </Dialog>
                </div>
            </div>
        </div>
    )
}
 
export default connect(r=>r)(Login);