import {Body, Controller, Post} from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from '../dto/user.dto';

@Controller('api')
export class UserController {
  constructor(private service: UserService) {}
  @Post('register')
 async register(@Body() body): Promise<any> {
    let user = await this.service.add(body)
    return console.log(user)
  }
}
